#! /bin/bash
####################################################
# https://www.youtube.com/midfingr                 # 
# script to enable colors in the terminal          #
# make sure the sripts are executable              #
# chmod +x colors.sh and chmod +x remove_colors.sh #                                             
####################################################

sudo -v

cp /home/$USER/.bashrc.backup /home/$USER/.bashrc
rm /home/$USER/.config/fish
sudo cp /etc/bash.bashrc.backup /etc/bash.bashrc
sudo rm /etc/DIR_COLORS

sudo pacman -R fish fortune-mod

echo "Bash de-colorized"
