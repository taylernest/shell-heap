#! /bin/bash
####################################################
# https://www.youtube.com/midfingr                 #
# script to enable colors in the terminal          #
# make sure the sripts are executable              #
# chmod +x colors.sh and chmod +x remove_colors.sh #
####################################################

fish --version

if [ $? != 0 ]; then
	sudo pacman -S fish fortune-mod
	if [ $? != 0 ]; then
		echo "Error: can't install required packages! Is sudo actually exists?"
		exit 1;
	fi
fi

cd ${BASH_SOURCE[0]}

cp /home/$USER/.bashrc /home/$USER/.bashrc.backup
cp bashrc /home/$USER/.bashrc
mkdir -p /home/$USER/.config/fish/
cp config.fish /home/$USER/.config/fish/
sudo cp /etc/bash.bashrc /etc/bash.bashrc.backup
sudo cp bash.bashrc /etc/bash.bashrc
sudo cp DIR_COLORS /etc/DIR_COLORS

echo "Bash colorized! Now you neet to restart your terminal"
