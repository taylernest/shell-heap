#!/bin/sh

# Pacaur half-automated installation script
# Author: TaylerNest
# License: The Unlicense

if [ "$EUID" == 0 ]; then
	echo "Should NOT be root!"
	exit 1
fi

which sudo > /dev/null

if [ $? != 0 ]; then
	echo "Sudo not found!"
	echo "Note that in order to install pacaur successfully you need to have sudo AND to be in sudoers list as well"
	exit 1
fi

sudo -v

echo
echo "======================"
echo "Resolving dependencies"
echo "======================"
echo 
sleep 3

sudo pacman -S binutils make gcc fakeroot --noconfirm
sudo pacman -S expac yajl git --noconfirm

mkdir -p /tmp/pacaur
cd /tmp/pacaur

echo 
echo "======================"
echo "Installing"
echo "======================"
echo 
sleep 3

curl -o PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=cower
makepkg PKGBUILD --skippgpcheck
sudo pacman -U cower*.tar.xz --noconfirm

curl -o PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=pacaur
makepkg PKGBUILD --skippgpcheck
sudo pacman -U pacaur*.tar.xz --noconfirm

cd ~
rm -r /tmp/pacaur

which pacaur > /dev/null

if [ $? != 0 ]; then
	echo "========================="
	echo "FAILED TO INSTALL PACAUR!"
	echo "========================="
	exit 1
fi

echo
echo 
echo "====================="
echo "PACAUR INSTALLED!"
echo "====================="
echo
sleep 1
