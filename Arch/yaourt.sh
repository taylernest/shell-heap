sudo -v

if [ $? != 0 ]; then
	echo "Sudo failed!"
	exit 1
fi

git --version > /dev/null
if [ $? != 0 ]; then
	sudo pacman -S git
fi

cd /tmp/
git clone https://aur.archlinux.org/package-query.git
cd package-query
makepkg -si
if [ $? != 0 ]; then
	cd ..
	rm -r package-query
	echo "Makepkg failed!"
	echo "Try installing through pacaur -S yaourt"
	exit 1
fi
cd ..
git clone https://aur.archlinux.org/yaourt.git
cd yaourt
makepkg -si
cd ..